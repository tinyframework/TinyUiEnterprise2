(function ($) {
    'use strict';

    $.extend($.fn.bootstrapTable.defaults, {
        dbClickEdit: false
    });
    var timer;
    var clikcToEdit = function (evt, tarNode, rowData) {
        clearTimeout(timer);
        var txt = [], table = evt, _table = evt.$el,currentClass='edit-row-doing';
        var currentIndex=table.$data.thId;
        if(tarNode.hasClass(currentClass)){
            return;
        }

        var replaceData = function () {
            var reObj = {};
            var showObj = {};
            if(!tarNode.hasClass(currentClass)){
                return false;
            }
            tarNode.find('td').find('input[type="text"]').each(function (i, td) {
                showObj[td.id] = reObj[td.id] = $(this).val();
            });

            tarNode.find('td').find('select').each(function (i, td) {
                reObj[td.id] = $(this).val();
                showObj[td.id] = $(this).find("option:selected").text();
            });
            _table.bootstrapTable('updateRow', {
                index: currentIndex,
                row: reObj
            });
            _table.trigger("edit.row.bs.table", [rowData[_table.data("idField")], reObj]);
            tarNode.removeClass(currentClass);
            return false;
        };


        $(document).off('click.bt-row-edit').on('click.bt-row-edit',function(e){
            clearTimeout(timer);
           if($(e.target).closest('tr').attr('data-index')!=currentIndex){
             timer=setTimeout(function(){
                 replaceData();
             },250);
           }
        });

        table.columns.forEach(function (column, i) {
            if (!column.editable) return;
            var dom = "";
            switch (column.editable) {
                case 'input':
                    dom = $('<input type="text" class="form-control input-sm" id="' + column.field + '"/>').val(rowData[column.field]);
                    break;
                case 'select':
                    dom = $('<select id="' + column.field + '" class="form-control input-sm">');
                    $.selectArray[column.field].forEach(function (item, i) {
                        var data = {"value": item.key};
                        if (item.key == rowData[column.field]) {
                            data["selected"] = "selected";
                        }
                        dom.append($("<option/>").attr(data).text(item.value));
                    });
                    break;
                case 'textarea':
                    dom = $('<textarea class="form-control input-sm" id="' + column.field + '"/>').val(rowData[column.field]);
                    break;
                default:
                    console.log(column.fieldIndex + ' ' + column.editable);
            }
            if (dom) {
                tarNode.find('td').eq(column.fieldIndex).text('').append(dom);
            }

        }, evt);
        for (var i = 0, l = txt.length; i < l; i++) {
            tarNode.find('input[type="text"]').eq(i).val(txt[i]);
        }
        _table.find('tr.'+currentClass).trigger('clear-edit');
        tarNode.off('clear-edit').on('clear-edit',replaceData);
        _table.find('tr').removeClass(currentClass);
        tarNode.addClass(currentClass);
    };

    var BootstrapTable = $.fn.bootstrapTable.Constructor,
        _initTable = BootstrapTable.prototype.initTable,
        _initBody = BootstrapTable.prototype.initBody;

    BootstrapTable.prototype.initTable = function () {
        var that = this;
        this.$data = {};
        _initTable.apply(this, Array.prototype.slice.apply(arguments));
    };

    BootstrapTable.prototype.initBody = function () {
        var that = this;
        _initBody.apply(this, Array.prototype.slice.apply(arguments));

        if (!this.options.dbClickEdit) {
            return;
        }
        var table = this.$tableBody.find('table');
        table.on('dbl-click-row.bs.table', function (e, row, $element, field) {
            this.$data.thId = $element.data().index;
            clikcToEdit(this, $element, row);
        }.bind(this));
    };
})(jQuery);